/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MySession;

import Model.MyRandomForestModel;

/**
 *
 * @author thanh
 */
public class Session {
    private static String trainningPath = "E:/Dat/IT/weka/Du doan ket qua hoc tap -Random forest/data-18.6.arff";
    private static MyRandomForestModel randomForestModel = new MyRandomForestModel(trainningPath);

    public static MyRandomForestModel getRandomForestModel() {
        return randomForestModel;
    }

    public static void setRandomForestModel(MyRandomForestModel randomForestModel) {
        Session.randomForestModel = randomForestModel;
    }

    
    public static String getTrainningPath() {
        return trainningPath;
    }

    public static void setTrainningPath(String trainningPath) {
        Session.trainningPath = trainningPath;
    }
    
          
}
