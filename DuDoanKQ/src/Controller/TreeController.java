/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.MyRandomForestModel;
import Model.Student;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.trees.RandomForest;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

/**
 *
 * @author thanh
 */
public class TreeController {
    public RandomForest CreateTree(String path){
        try {
            ConverterUtils.DataSource src = new ConverterUtils.DataSource(path);
            Instances dt = src.getDataSet();
            dt.setClassIndex(dt.numAttributes() - 1);

            String[] options = new String[4];
            options[0] = "-C";
            options[1] = "0.1";
            options[2] = "-M";
            options[3] = "2";
            RandomForest mytree = new RandomForest();
            mytree.setOptions(options);
            mytree.buildClassifier(dt);
             System.out.println(mytree.getTechnicalInformation());
             return mytree;
            //weka.core.SerializationHelper.write("E:\\Dat\\IT\\myDT.model.txt", mytree);
        }
        catch (Exception e) {
            System.out.println("Error!!!!\n" + e.getMessage());
            return null;
        }
    }
    public float GetDiemDuKien(String path, Student std){
        try {
            MyRandomForestModel randomForest = new MyRandomForestModel(path);
            Instance testInstance = randomForest.getTestInstance(std);
            RandomForest tree = randomForest.performTraining();
            int purpose = (int) tree.classifyInstance(testInstance);
            String resultspurpose = randomForest.trainingData.attribute(4).value(purpose);
            float DiemDuKien = Float.parseFloat(resultspurpose);
            return DiemDuKien;
        } catch (Exception ex) {
            Logger.getLogger(TreeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    public float GetDiemDuKien(MyRandomForestModel randomForest, Student std){
        try {
            //MyRandomForestModel randomForest = new MyRandomForestModel(path);
            Instance testInstance = randomForest.getTestInstance(std);
            RandomForest tree = randomForest.performTraining();
            int purpose = (int) tree.classifyInstance(testInstance);
            String resultspurpose = randomForest.trainingData.attribute(4).value(purpose);
            float DiemDuKien = Float.parseFloat(resultspurpose);
            return DiemDuKien;
        } catch (Exception ex) {
            Logger.getLogger(TreeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
}
