/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author thanh
 */
public class Student {

    private int GioiTinh;
    private double TBCKy1;
    private double ThoiGianHocKy1;
    private double ThoiGianHocKy2;
    private double TBCKy2;

    public int getGioiTinh() {
        return GioiTinh;
    }

    public void setGioiTinh(int GioiTinh) {
        this.GioiTinh = GioiTinh;
    }

    public Student(int GioiTinh, double TBCKy1, double ThoiGianHocKy1, double ThoiGianHocKy2, double TBCKy2) {
        this.GioiTinh = GioiTinh;
        this.TBCKy1 = TBCKy1;
        this.ThoiGianHocKy1 = ThoiGianHocKy1;
        this.ThoiGianHocKy2 = ThoiGianHocKy2;
        this.TBCKy2 = TBCKy2;
    }

    public Student() {
    }
    

    public double getTBCKy1() {
        return TBCKy1;
    }

    public void setTBCKy1(double TBCKy1) {
        this.TBCKy1 = TBCKy1;
    }

    public double getThoiGianHocKy1() {
        return ThoiGianHocKy1;
    }

    public void setThoiGianHocKy1(double ThoiGianHocKy1) {
        this.ThoiGianHocKy1 = ThoiGianHocKy1;
    }

    public double getThoiGianHocKy2() {
        return ThoiGianHocKy2;
    }

    public void setThoiGianHocKy2(double ThoiGianHocKy2) {
        this.ThoiGianHocKy2 = ThoiGianHocKy2;
    }

    public double getTBCKy2() {
        return TBCKy2;
    }

    public void setTBCKy2(double TBCKy2) {
        this.TBCKy2 = TBCKy2;
    }
    
}
