/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import weka.classifiers.trees.RandomForest;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author thanh
 */
public class MyRandomForestModel {
     public Instances trainingData;
     public MyRandomForestModel(String fileName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            trainingData = new Instances(reader);
            trainingData.setClassIndex(trainingData.numAttributes() - 1);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
     public RandomForest performTraining() {
        RandomForest randomForest = new RandomForest();
        String[] options = {"-U"};
//        Use unpruned tree. -U
        try {
            //randomForest.setOptions(options);
            randomForest.buildClassifier(trainingData);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return randomForest;
    }
     public Instance getTestInstance(
            String binding, String multicolor, String genre) {
        Instance instance = new DenseInstance(3);
        instance.setDataset(trainingData);
        instance.setValue(trainingData.attribute(0), binding);
        instance.setValue(trainingData.attribute(1), multicolor);
        instance.setValue(trainingData.attribute(2), genre);
        return instance;
    }

    public Instance getTestInstance(int a, double b, double c, double d) {
        Instance instance = new DenseInstance(4);
        instance.setDataset(trainingData);
        instance.setValue(trainingData.attribute(0), a);
        instance.setValue(trainingData.attribute(1), b);
        instance.setValue(trainingData.attribute(2), c);
        instance.setValue(trainingData.attribute(3), d);

        return instance;
    }
     public Instance getTestInstance(Instances trainingData, Student student) {
        Instance instance = new DenseInstance(6);
        instance.setDataset(trainingData);
        instance.setValue(trainingData.attribute(0), student.getGioiTinh());
        instance.setValue(trainingData.attribute(1), student.getTBCKy1());
        instance.setValue(trainingData.attribute(2), student.getThoiGianHocKy1());
        instance.setValue(trainingData.attribute(3), student.getThoiGianHocKy2());
   
        return instance;
    }
     public Instance getTestInstance(Student student) {
        Instance instance = new DenseInstance(6);
        instance.setDataset(trainingData);
        instance.setValue(trainingData.attribute(0), student.getGioiTinh());
        instance.setValue(trainingData.attribute(1), student.getTBCKy1());
        instance.setValue(trainingData.attribute(2), student.getThoiGianHocKy1());
        instance.setValue(trainingData.attribute(3), student.getThoiGianHocKy2());
       

        return instance;
    }
     
     
}
